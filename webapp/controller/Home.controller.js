sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/UIComponent",
	"../model/formatter"
], function(Controller, JSONModel, UIComponent, formatter) {
	"use strict";

	return Controller.extend("sap.ui.demo.basicTemplate.controller.App", {

		_sUrl: "https://fakerestapi.azurewebsites.net/api/v1/",
		formatter: formatter,
		

		onInit: function () {
			this._oRouter = UIComponent.getRouterFor(this);
			this._oRouter.getRoute("home").attachPatternMatched(this._onObjectMatched, this);
			this.getView().setModel(new JSONModel({}), "autorModel");
			this.getView().setModel(new JSONModel({
				"showBooks": false
			}), "config");			
		},

		onChangeComboBoxAutors: function(oEvent) {
			var sPath = oEvent.getParameter("selectedItem").getBindingContext("autorModel").getPath();
			var oModel = this.getView().getModel("autorModel");
			var oAutor = oModel.getProperty(sPath);
			oModel.setProperty("/currentAutor", oAutor);
			this.getView().getModel("config").setProperty("/showBooks", true);
			this._loadBooks(oAutor);
		},

		_onObjectMatched: function() {
			this._loadAutors();
		},

		_loadAutors: function() {
			this.getView().setBusy(true);
			var that = this;
			jQuery.ajax({
				"url": this._sUrl + "Authors",
				"method": "GET",
				"data": {},
				"contentType": "application/json;charset=UTF-8",
				"context": this
			}).done(function(aData) {
				var oModel = that.getView().getModel("autorModel");
				oModel.setSizeLimit(aData.length);
				oModel.setProperty("/autors", aData);	
				this.getView().setBusy(false);			
			}).fail(function(oError) {
				console.error(oError);
				this.getView().setBusy(false);
			});			
		},

		_loadBooks: function(oAutor) {
			var oComboBox = this.byId("comboBoxBooks");
			oComboBox.setBusy(true);
			var that = this;
			jQuery.ajax({
				"url": this._sUrl + "Books/" + oAutor.idBook,
				"method": "GET",
				"data": {},
				"contentType": "application/json;charset=UTF-8",
				"context": this
			}).done(function(oData) {
				that.getView().getModel("autorModel").setProperty("/books", [oData]);	
				oComboBox.setBusy(false);			
			}).fail(function(oError) {
				console.error(oError);
				oComboBox.setBusy(false);			
			});			
		}
	});
});