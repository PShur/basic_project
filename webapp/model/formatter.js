sap.ui.define([], function () {
	"use strict";
	return {
		formatFullName: function(sFirstName, sLastName) {
			return sFirstName + " " + sLastName;
		},
	};
});